from django.db import models
from model_utils.models import TimeStampedModel
from model_utils import Choices
from django.core.validators import MinValueValidator


class Player(TimeStampedModel):
    """
    Fantasy Sports players and their salaries for a given player lineup challenge
    """
    lineupChallenge = models.ForeignKey('FantasySportPlayerLineupChallenge', null=False, related_name='players')
    firstName = models.CharField(max_length=50, null=False, blank=False, verbose_name="The player's first name")
    lastName = models.CharField(max_length=50, null=False, blank=False, verbose_name="The player's last name")
    salary = models.DecimalField(max_digits=20, decimal_places=2, null=False, blank=False,
                                 validators=[MinValueValidator(0.0)], verbose_name="The player's salary in dollars")
    POSITIONS = Choices(
        ("QB", "Quarterback"),
        ("RB", "Runningback"),
        ("WR", "Wide Receiver"),
        ("TE", "Tightend"),
    )
    position = models.CharField(max_length=20, null=False, blank=False,
                                choices=POSITIONS, verbose_name="The players position")

    lineup = models.ForeignKey('FantasySportPlayerLineupChallenge', null=True, editable=False, related_name='lineup')

    @property
    def name(self):
        return '%s %s' % (self.firstName, self.lastName)

    class Meta:
        app_label = "dfs_lineup"
        ordering = ['lineupChallenge', 'position', '-salary']

    def __unicode__(self):
        return '%s[%s] $%d' % (self.name, self.position, self.salary)


class FantasySportPlayerLineupChallenge(TimeStampedModel):
    """
    Inputs: A list of Fantasy Sports players and their salaries plus a salary cap.
    Output: An ordered list of an optimal football lineup, optimizing for the constraints
    """
    LINEUP_POSITIONS = ["QB", "RB1", "RB2", "WR1", "WR2", "TE", "FLEX"]
    salaryCap = models.DecimalField(max_digits=22, decimal_places=2, null=False, blank=False,
                                    validators=[MinValueValidator(0.0)], verbose_name="The total salary cap, in dollars")

    salaryCapRemaining = models.DecimalField(max_digits=22, decimal_places=2, null=True, editable=False,
                                             validators=[MinValueValidator(0.0)],
                                             verbose_name="The remaining salary cap, in dollars")
    class Meta:
        app_label = "dfs_lineup"
        ordering = ['-id']

    def __unicode__(self):
        return 'Lineup challenge[%d] - Salary Cap $%d' % (self.id, self.salaryCap)
