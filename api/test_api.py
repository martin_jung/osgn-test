from django import test
from django.test.client import Client
from django.contrib.auth.models import User
from fixtures import lineupChallengeData
from rest_framework import status
import json


class ApiTestCase(test.TestCase):
    def setUp(self):
        self.user = User.objects.create_user('api', 'api@api.ca', 'api')
        self.client = Client()
        self.client.login(username='api', password='api')

    def _post(self, uri, data):
        """Uses self.client to POST to uri with JSON data."""
        response = self.client.post(
            uri,
            data=json.dumps(data),
            content_type='application/json'
        )
        return response.status_code, json.loads(response.content)

    def test_empty_fields(self):
        """
        Verify that submitting all fields as empty works as expected.
        """
        uri = '/api/lineup_challenges/'
        status_code, errors = self._post(uri, lineupChallengeData)
        self.assertEqual(status_code, status.HTTP_200_OK)
