from dfs_lineup.models import *
from rest_framework import serializers
from rest_framework_bulk import BulkListSerializer, BulkSerializerMixin

class PlayerSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Player
        list_serializer_class = BulkListSerializer
        fields = ('firstName', 'lastName', 'salary', 'position')

    def to_internal_value(self, data):
        retVal = super(PlayerSerializer, self).to_internal_value(data)
        retVal['lineupChallenge_id'] = data['lineupChallenge_id']
        return retVal


class FantasySportPlayerLineupChallengeSerializer(serializers.ModelSerializer):
    lineup = PlayerSerializer(many=True, read_only=True)
    class Meta:
        model = FantasySportPlayerLineupChallenge
        fields = ('id', 'salaryCap', 'salaryCapRemaining', 'lineup')
