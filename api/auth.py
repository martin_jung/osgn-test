from django.contrib.auth.models import User
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.conf import settings


class LocalTokenAuthentication(BaseAuthentication):
    """
    Checks the header for X_TOKEN field and compares it against the
    local-www-key specified in settings_default.py
    """
    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        if not token:
            return None

        if token[6:] == settings.LOCAL_WWW_KEY:
            try:
                user = User.objects.get(username='dfs_lineup_api')
                return (user, None)
            except User.DoesNotExist:
                raise AuthenticationFailed('User dfs_lineup_api does not exist.')
        else:
            raise AuthenticationFailed('Key mismatch')

