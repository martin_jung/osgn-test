from rest_framework import viewsets
from api.serializers import *
from rest_framework_bulk import ListBulkCreateUpdateDestroyAPIView
from rest_framework.request import Request
from rest_framework.exceptions import ValidationError, APIException


class PlayerView(ListBulkCreateUpdateDestroyAPIView):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class FantasySportPlayerLineupChallengeViewSet(viewsets.ModelViewSet):
    queryset = FantasySportPlayerLineupChallenge.objects.all()
    serializer_class = FantasySportPlayerLineupChallengeSerializer

    def modify_request(self, lineupChallenge_id):
        """
        Internal helper method to clone a request, replacing with a different
        HTTP method.  Used for checking permissions against other methods.
        """
        request = self.request
        method = 'POST'
        ret = Request(request=request._request,
                      parsers=request.parsers,
                      authenticators=request.authenticators,
                      negotiator=request.negotiator,
                      parser_context=request.parser_context)
        try:
            players = request._data['players']
        except KeyError:
            raise ValidationError("players not exist.")
        for player in players:
            player['lineupChallenge_id'] = lineupChallenge_id
        ret._data = players
        ret._full_data = request._data['players']
        ret._content_type = request._content_type
        ret._method = method
        return ret

    def categorizePlayerPoolAndSortBySalaryDec(self, lineupChallenge_id):
        self.challenge = FantasySportPlayerLineupChallenge.objects.get(pk=lineupChallenge_id)
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            setattr(self, position, [])
        for player in Player.objects.filter(lineupChallenge__id=lineupChallenge_id):
            if player.position == 'QB':
                getattr(self, player.position, []).append(player)
            elif player.position == 'TE':
                getattr(self, player.position, []).append(player)
                getattr(self, "FLEX", []).append(player)
            else:
                getattr(self, "%s1" % player.position, []).append(player)
                getattr(self, "%s2" % player.position, []).append(player)
                getattr(self, "FLEX", []).append(player)
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            setattr(self, position, sorted(getattr(self, position, []), key=lambda x: x.salary, reverse=True))
        self.number_of_players_in_positions = {'max': 0}
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            number_of_players_in_position = len(getattr(self, position, []))
            self.number_of_players_in_positions[position] = number_of_players_in_position
            if number_of_players_in_position == 0:
                raise ValidationError("There in no %s in players pool." % position)
            elif number_of_players_in_position > self.number_of_players_in_positions['max']:
                self.number_of_players_in_positions['max'] = number_of_players_in_position

    def roughlyCutPlayersByCap(self):
        for index in range(0, self.number_of_players_in_positions['max'], 1):
            self.salaryCapRemaining = self.challenge.salaryCap
            self.lineup_pool = []
            self.roughCutIndexes = {}
            for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
                for sub_index in range(index, self.number_of_players_in_positions[position], 1):
                    player = getattr(self, position, [])[sub_index]
                    if player not in self.lineup_pool:
                        self.lineup_pool.append(player)
                        self.roughCutIndexes[position] = sub_index
                        self.salaryCapRemaining -= getattr(self, position, [])[sub_index].salary
                        break
            if self.salaryCapRemaining >= 0 and len(self.lineup_pool) == 7:
                self.salaryCapRemaining = self.salaryCapRemaining
                break
        if len(self.lineup_pool) < 7 or self.salaryCapRemaining < 0:
            raise APIException("Cannot make a lineup with salary cap.")

    def fineCutAfterRoughCut(self):
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            if self.roughCutIndexes[position] > 0:
                diff = getattr(self, position, [])[self.roughCutIndexes[position] - 1].salary \
                                           - getattr(self, position, [])[self.roughCutIndexes[position]].salary
                if self.salaryCapRemaining >= diff and \
                        getattr(self, position, [])[self.roughCutIndexes[position] - 1] not in self.lineup_pool:
                    self.salaryCapRemaining -= diff
                    self.lineup_pool.remove(getattr(self, position, [])[self.roughCutIndexes[position]])
                    self.lineup_pool.append(getattr(self, position, [])[self.roughCutIndexes[position] - 1])
                else:
                    return
            else:
                continue
        self.fineCutAfterRoughCut()

    def retrieve(self, request, *args, **kwargs):
        self.categorizePlayerPoolAndSortBySalaryDec(kwargs.get('pk', None))
        self.roughlyCutPlayersByCap()
        self.fineCutAfterRoughCut()
        self.challenge.salaryCapRemaining = self.salaryCapRemaining
        self.challenge.save()
        for player in self.lineup_pool:
            player.lineup_id = kwargs.get('pk', None)
            player.save()
        return super(FantasySportPlayerLineupChallengeViewSet, self).retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        retVal = super(FantasySportPlayerLineupChallengeViewSet, self).create(request, *args, **kwargs)
        lineupChallenge_id = retVal.data['id']
        kwargs['lineupChallenge_id'] = lineupChallenge_id
        modified_request = self.modify_request(lineupChallenge_id)
        playerView = PlayerView(request=modified_request)
        playerView.format_kwarg = self.format_kwarg
        if lineupChallenge_id:
            old_player_ids = [player.id for player in Player.objects.filter(lineupChallenge__id=lineupChallenge_id)]
        try:
            playerView.create(modified_request, *args, **kwargs)
        except Exception as e:
            chanllenge = FantasySportPlayerLineupChallenge.objects.get(pk=lineupChallenge_id)
            chanllenge.delete()
            raise e
        for player in Player.objects.filter(id__in=old_player_ids):
            player.delete()
        kwargs['pk'] = kwargs.pop('lineupChallenge_id', None)
        self.kwargs = kwargs
        return self.retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        super(FantasySportPlayerLineupChallengeViewSet, self).update(request, *args, **kwargs)
        lineupChallenge_id = kwargs.pop('pk', None)
        kwargs['lineupChallenge_id'] = lineupChallenge_id
        modified_request = self.modify_request(lineupChallenge_id)
        playerView = PlayerView(request=modified_request)
        playerView.format_kwarg = self.format_kwarg
        if lineupChallenge_id:
            old_player_ids = [player.id for player in Player.objects.filter(lineupChallenge__id=lineupChallenge_id)]
        playerView.create(modified_request, *args, **kwargs)
        for player in Player.objects.filter(id__in=old_player_ids):
            player.delete()
        kwargs['pk'] = lineupChallenge_id
        self.kwargs = kwargs
        return self.retrieve(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        retVal = super(FantasySportPlayerLineupChallengeViewSet, self).destroy(request, *args, **kwargs)
        lineupChallenge__id = kwargs.pop('pk', None)
        if lineupChallenge__id:
            for player in Player.objects.filter(lineupChallenge__id=lineupChallenge__id):
                player.delete()
        return retVal

